package com.example.cse438.studio6.db

import android.provider.BaseColumns

class DbSettings {
    companion object {
        const val DB_NAME = "favourites.db"
        const val DB_VERSION = 1
    }

    class DBFavoriteEntry: BaseColumns {
        companion object {
            const val TABLE = "favorites"
            const val ID = BaseColumns._ID
            const val COL_API_ID = "api_id"
            const val COL_FAV_TITLE = "title"
            const val COL_DESCRIPTION = "description"
            const val COL_MANUFACTURER = "manufacturer"
            const val COL_MODEL = "model"
            const val COL_BRAND = "brand"
            const val COL_CATEGORY_ID = "category_id"
            const val COL_CATEGORY_NAME = "category_name"
            const val COL_WEIGHT = "weight"
            const val COL_LENGTH = "length"
            const val COL_WIDTH = "width"
            const val COL_HEIGHT = "height"
            const val COL_IMAGE_COUNT = "image_count"
            const val COL_PRICE = "price"
            const val COL_CURRENCY = "currency"
            const val COL_COLOR = "color"
            const val COL_IS_NEW = "is_new"
            const val COL_UPC = "upc"
            const val COL_EAN = "ean"
            const val COL_MPN = "mpn"
            const val COL_CREATED_DATE = "created_date"
            const val COL_UPDATED_DATE = "updated_date"
            const val COL_SEM3_ID = "sem3_id"
        }
    }

    class DBImageAssetEntry: BaseColumns {
        companion object {
            const val TABLE = "images"
            const val ID = BaseColumns._ID
            const val FAVORITE_ID = "favorite_id"
            const val COL_URL = "cloud_url"
        }
    }

    class DBFeatureEntry: BaseColumns {
        companion object {
            const val TABLE = "features"
            const val ID = BaseColumns._ID
            const val FAVORITE_ID = "favorite_id"
            const val COL_NAME = "name"
            const val COL_DESCRIPTION = "description"
        }
    }

    class DBGtinEntry: BaseColumns {
        companion object {
            const val TABLE = "gtins"
            const val ID = BaseColumns._ID
            const val FAVORITE_ID = "favorite_id"
            const val COL_GTIN = "gtin"
        }
    }

    class DBVariationSecondaryIdEntry: BaseColumns {
        companion object {
            const val TABLE = "variation_secondary_ids"
            const val ID = BaseColumns._ID
            const val FAVORITE_ID = "favorite_id"
            const val COL_VAR_SEC_ID = "variation_secondary_id"
        }
    }

    class DBGeographyEntry: BaseColumns {
        companion object {
            const val TABLE = "geography"
            const val ID = BaseColumns._ID
            const val FAVORITE_ID = "favorite_id"
            const val COL_GEO = "geography"
        }
    }

    class DBMessageEntry: BaseColumns {
        companion object {
            const val TABLE = "messages"
            const val ID = BaseColumns._ID
            const val FAVORITE_ID = "favorite_id"
            const val COL_MESSAGE = "message"
        }
    }

    class DBSiteDetailEntry: BaseColumns {
        companion object {
            const val TABLE = "site_details"
            const val ID = BaseColumns._ID
            const val FAVORITE_ID = "favorite_id"
            const val COL_NAME = "name"
            const val COL_URL = "url"
            const val COL_SKU = "sku"
            const val COL_OFFERS_COUNT = "offers_count"
        }
    }

    class DBOfferEntry: BaseColumns {
        companion object {
            const val TABLE = "offers"
            const val ID = BaseColumns._ID
            const val SITE_DETAIL_ID = "site_detail_id"
            const val COL_API_ID = "api_id"
            const val COL_PRICE = "price"
            const val COL_CURRENCY = "currency"
            const val COL_SELLER = "seller"
            const val COL_CONDITION = "condition"
            const val COL_AVAILABILITY = "availability"
            const val COL_IS_ACTIVE = "is_active"
            const val COL_FIRST_RECORDED = "first_recorded"
            const val COL_LAST_RECORDED = "last_recorded"
        }
    }
}