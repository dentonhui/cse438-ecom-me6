package com.example.cse438.studio6.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import com.example.cse438.studio6.R
import com.example.cse438.studio6.activity.MainActivity
import com.example.cse438.studio6.enum.UserInterfaceState
import com.example.cse438.studio6.model.Product
import kotlinx.android.synthetic.main.fragment_home.*

// TODO: Note that this class now optionally accepts a list of products
@SuppressLint("ValidFragment")
class HomeFragment(context: Context, private var productList: ArrayList<Product>? = null): Fragment() {
    private var parentContext = context
    private var initialized: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onStart() {
        super.onStart()

        if (!this.initialized) {
            val fm = fragmentManager
            val ft = fm?.beginTransaction()

            // TODO: Note that if the product list passed to this class is null, we want to show the home page and if not we assume we are coming here from scanning barcodes
            if (this.productList == null) {
                ft?.add(R.id.list_holder, WhatsNewFragment(this.parentContext), "NEW_FRAG")
            }
            else {

                ft?.add(R.id.list_holder, ResultListFragment(this.parentContext, products = productList), "RESULTS_FRAG")
            }
            ft?.commit()

            search_edit_text.setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    val searchText = search_edit_text.text
                    search_edit_text.setText("")
                    if (searchText.toString() == "") {
                        val toast = Toast.makeText(this.parentContext, "Please enter text", Toast.LENGTH_SHORT)
                        toast.setGravity(Gravity.CENTER, 0, 0)
                        toast.show()
                        return@setOnEditorActionListener true
                    }
                    else {
                        performSearch(searchText.toString())
                        return@setOnEditorActionListener false
                    }
                }

                return@setOnEditorActionListener false
            }

            this.initialized = true
        }
    }

    private fun performSearch(query: String) {
        (activity as MainActivity).currentView = UserInterfaceState.RESULTS

        // Load Fragment into View
        val fm = fragmentManager

        // add
        val fragment = ResultListFragment(this.parentContext, query)
        val ft = fm?.beginTransaction()
        ft?.replace(R.id.list_holder, fragment, "RESULTS_FRAG")
        ft?.commit()
    }
}